#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

void buatTim(int a, int b, int c) {
    char filename[50];
    sprintf(filename, "Formasi_%d-%d-%d.txt", a, b, c);
    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("Error creating file!\n");
        return;
    }
    fclose(fp);
}

int main(){
    pid_t child_download, child_unzip, child_remove_pemain, child_remove_selain_mu;
    int status;
    child_download = fork();
    if (child_download == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "-O", "pemain", NULL};

        execv("/usr/bin/wget", argv);
    }

    child_unzip = fork();
    if (child_unzip == 0)
    {
        sleep(3);
        char *argv[]={"unzip", "pemain",NULL};
        execv("/bin/unzip", argv);
    }

    child_remove_pemain = fork();
    if (child_remove_pemain < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_remove_pemain == 0 ){
        sleep(4);
        char *argv[] = {"rm", "-rf", "pemain", NULL};
        execv("/bin/rm", argv);
    }


    child_remove_selain_mu = fork();
    if (child_remove_selain_mu < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_remove_selain_mu == 0 ){
        sleep(4);
        
        char *argv[] = {"find", "players", "-type", "f", "!", "-name", "*ManUtd*.png", "-delete", NULL};
        execv("/bin/find", argv);
    }


    pid_t child_createdir_kiper, child_createdir_bek, child_createdir_gelandang, child_createdir_penyerang;

    child_createdir_kiper = fork();
    if (child_createdir_kiper < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_kiper == 0 )
    {
        char *argv[] = {"mkdir", "-p", "players/kiper",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_bek = fork();
    if (child_createdir_bek < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_bek == 0 )
    {
        char *argv[] = {"mkdir", "-p", "players/bek",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_gelandang = fork();
    if (child_createdir_gelandang < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_gelandang == 0 )
    {
        char *argv[] = {"mkdir", "-p", "players/gelandang",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_penyerang = fork();
    if (child_createdir_penyerang < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_penyerang == 0 )
    {
        char *argv[] = {"mkdir", "-p", "players/penyerang",NULL};
        execv("/bin/mkdir", argv);
    }

    pid_t child_move_kiper, child_move_bek, child_move_gelandang, child_move_penyerang;
    while (wait(NULL) > 0);

    child_move_kiper = fork();
    if (child_move_kiper < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_move_kiper == 0 )
    {
        char *argv[] = {"find", "players", "-type", "f", "-iname", "*Kiper*", "-exec", "mv", "{}", "players/kiper", ";", NULL};
		execv("/bin/find", argv);
    }
    else
    {
        while(wait(&status) > 0 );
    }

    child_move_bek = fork();
    if (child_move_bek < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_move_bek == 0 )
    {
        char *argv[] = {"find", "players", "-type", "f", "-iname", "*Bek*", "-exec", "mv", "{}", "players/bek", ";", NULL};
		execv("/bin/find", argv);
    }
    else
    {
        while(wait(&status) > 0 );
    }

    child_move_gelandang = fork();
    if (child_move_gelandang < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_move_gelandang == 0 )
    {
        char *argv[] = {"find", "players", "-type", "f", "-iname", "*Gelandang*", "-exec", "mv", "{}", "players/gelandang", ";", NULL};
		execv("/bin/find", argv);
    }
    else
    {
        while(wait(&status) > 0 );
    }

    child_move_penyerang = fork();
    if (child_move_penyerang < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_move_penyerang == 0 )
    {
        char *argv[] = {"find", "players", "-type", "f", "-iname", "*Penyerang*", "-exec", "mv", "{}", "players/penyerang", ";", NULL};
		execv("/bin/find", argv);
    }
    else
    {
        while(wait(&status) > 0 );
    }

    return 0;
}

