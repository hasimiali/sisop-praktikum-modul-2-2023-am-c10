#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <signal.h>

int main(int argc, char** argv) {

    char path[100];
    int hours, minutes, seconds;

    if (argc == 5){
        if (!strcmp(argv[1], "*")){
            seconds=-1;
            //printf("%d\n", seconds);
        }
        else{
            seconds=atoi(argv[1]);
            //printf("%d\n", seconds);
            if (seconds>= 60){
                printf("Error Format");
                exit(0);
            }
        }

        if (!strcmp(argv[2], "*")){
            minutes=-1;
            //rintf("%d\n", minutes);
        }
        else{
            minutes=atoi(argv[2]);
            //printf("%d\n", minutes);
            if (minutes >= 60){
                printf("Error Format");
                exit(0);
            }
        }
        //printf("%s", argv[3]);
        if (!strcmp(argv[3], "*")){
            hours=-1;
            //printf("%d\n", hours);
        }
        else{
            hours=atoi(argv[3]);
            //printf("%d\n", hours);
            if (hours >= 24){
                printf("Error Format");
                exit(0);
            }
        } 

        strcpy(path,argv[4]);
        //printf("path : %s\n", path);

        pid_t pid, sid;

        pid = fork();

        if (pid < 0) {
            exit(EXIT_FAILURE);
        }
        
        if (pid > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);

        sid = setsid();
        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

    }else{
        printf("Error Format");
        exit(0);
    } 

    while(1){
        time_t now = time(NULL);
        struct tm* time_now;
        time_now = localtime(&now);
        int checker=1;
        //printf("%d %d %d", time_now->tm_hour, time_now->tm_min, time_now->tm_sec);
        if (hours == -1 && minutes == -1 && seconds == -1){
            checker=1;
        }else {
            if(hours != -1){
                if(time_now->tm_hour != hours) checker=0;
            }
            if (minutes != -1){
                if(time_now->tm_min != minutes) checker=0;
            }
            if (seconds != -1){
                if(time_now->tm_sec != seconds) checker=0;
            }
        }
        //printf("Checker : %d\n", checker);
        if (checker > 0){
            pid_t make_child_id = fork();
            if (make_child_id < 0) {
                exit(EXIT_FAILURE);
            }
            if (make_child_id == 0){
                char * arg[] = {"bash", path, NULL};
                execv("/bin/bash", arg);
            }
        }
    }
    
}