#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <signal.h>


int main(int argc, char** argv) {

     if (argc == 2) {
        FILE* kill_soal2 = fopen("kill.sh", "w");
        fprintf(kill_soal2, "#!/bin/bash\n");
        if (!strcmp("-a", argv[1])) {
            fprintf(kill_soal2, "killall -9 lukisan\nrm $0\n");
        } else if (!strcmp("-b", argv[1])) {
            fprintf(kill_soal2, "kill_parent(){\n");
		    fprintf(kill_soal2, "kill ${@: -1}\n");
		    fprintf(kill_soal2, "}\n");
		    fprintf(kill_soal2, "kill_parent $(pidof lukisan)\nrm $0\n");
        }

        fclose(kill_soal2);
        
        pid_t cid = fork();
        if (cid < 0){
            exit(EXIT_FAILURE);
        }
        if (cid == 0){
            execlp("bin/chmod", "chmod", "+x", "kill.sh", NULL);
            exit(0);
        }

        pid_t pid, sid;

        pid = fork();

        if (pid < 0) {
            exit(EXIT_FAILURE);
        }
        
        if (pid > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);

        sid = setsid();
        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

    }

    while (1) {
        pid_t mkdir_child_id;

        //Direktori setiap 30 detik
        time_t now = time(NULL);
        time(&now);
        struct tm* time_info;
        time_info = localtime(&now);
        char timestamp[60];
        strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);
        //printf("timestamp : %s\n", timestamp);

        mkdir_child_id = fork();

        if (mkdir_child_id < 0) 
            exit(EXIT_FAILURE);

        if (mkdir_child_id == 0) {
            execlp("/bin/mkdir", "mkdir", timestamp, NULL);
            exit(0);
        }
        else {
            pid_t predownload_id = fork();
            if (predownload_id < 0){
                exit(EXIT_FAILURE);
            }
            if (predownload_id == 0){
                int i;
                for (i = 0; i < 15; i++)
                //download 15 gambar dengan looping
                {
                    pid_t download_id = fork();

                    if (download_id < 0){
                        exit(EXIT_FAILURE);
                    }

                    if (download_id == 0) {
                        now = time(NULL);
                        
                        char str[5];
                        char link[60];
                        sprintf(link, "https://picsum.photos/%ld", (now%1000)+50);
                        //printf("link : %s\n", link);

                        time_t time_pict;
                        struct tm *time_now_pict;
                        char files[30];
                        char down_dir[150];

                        time(&time_pict);
                        time_now_pict = localtime(&time_pict);

                        strftime(files, sizeof(files), "%Y-%m-%d_%H:%M:%S", time_now_pict);
                        sprintf(down_dir, "%s/%s", timestamp, files);

                        // printf("%s\n", down_dir);
                        char *arg[] = {"wget", "-q", "-O", down_dir, link, NULL};
                        execv("/bin/wget", arg);              
                    }
                    sleep(5); 
                }
                int status;
                wait(&status);
                char zip_name[100];
                sprintf(zip_name, "%s.zip", timestamp);
                pid_t zip_id = fork();

                if (zip_id < 0){
                    exit(EXIT_FAILURE);
                }
                if (zip_id == 0){
                    char *arg[] = {"zip", zip_name, "-rm", timestamp, NULL};
                    execv("/bin/zip", arg);
                }
                exit(0);
            }
            
        }
        sleep(30);
    }

}
