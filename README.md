Soal Shift Modul 2
Sistem Operasi 2023

Waktu pengerjaan dimulai Senin (20/3) pukul 10.00 WIB hingga Sabtu (25/3) pukul 22.00 WIB.
Praktikan diharapkan membuat laporan penjelasan dan penyelesaian soal dalam bentuk Readme(gitlab).
Format nama repository gitlab “sisop-praktikum-modul-[Nomor Modul]-2023-[Kode Dosen Kelas]-[Nama Kelompok]” (contoh:sisop-praktikum-modul-2-2023-WS-A11).
Struktur repository seperti berikut:
			---soal1:
				---binatang.c
			---soal2:
				---lukisan.c
			---soal3:
                ---filter.c
            ---soal4:
                ---mainan.c
	Jika melanggar struktur repo akan dianggap sama dengan curang dan menerima konsekuensi sama dengan melakukan kecurangan.
Setelah pengerjaan selesai, semua script bash, awk, dan file yang berisi cron job ditaruh di gitlab masing - masing kelompok, dan link gitlab diletakkan pada form yang disediakan.
Commit terakhir maksimal 10 menit setelah waktu pengerjaan berakhir. Jika melewati maka akan dinilai berdasarkan commit terakhir.
Jika tidak ada pengumuman perubahan soal oleh asisten, maka soal dianggap dapat diselesaikan.
Jika ditemukan soal yang tidak dapat diselesaikan, harap menuliskannya pada Readme beserta permasalahan yang ditemukan.
Praktikan tidak diperbolehkan menanyakan jawaban dari soal yang diberikan kepada asisten maupun praktikan dari kelompok lainnya.
Jika ditemukan indikasi kecurangan dalam bentuk apapun di pengerjaan soal shift, maka nilai dianggap 0.
Pengerjaan soal shift sesuai dengan modul yang telah diajarkan.
Zip dari repository dikirim ke email asisten penguji dengan subjek yang sama dengan nama judul repository, dikirim sebelum deadline dari soal shift

# Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

a. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

b. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

c. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

d. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan : 
untuk melakukan zip dan unzip tidak boleh menggunakan system

# Soal 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

d. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan :
Tidak boleh menggunakan system()
Proses berjalan secara daemon
Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

# Soal 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

a. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

b. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  

c. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

d. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

Catatan:
Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
Tidak boleh menggunakan system()
Tidak boleh memakai function C mkdir() ataupun rename().
Gunakan exec() dan fork().
Directory “.” dan “..” tidak termasuk yang akan dihapus.
Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

# Soal 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

a. Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

b. Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

c. Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

d. Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

e. Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

Bonus poin apabila CPU state minimum.
Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

# Jawaban
## Soal 1
### a
`
Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
`

pertama kita melakukan download file yang telah disediakan
```c
    child_id1 = fork();
    if (child_id1 == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "hewan", NULL};

        execv("/usr/bin/wget", argv);
    }
```
kemudian buat directory download
```c
    child_id2 = fork();
    if (child_id2 == 0)
    {
        sleep(2);
        char *argv[] = {"mkdir", "download",NULL};
        execv("/bin/mkdir", argv);
    }
```
unzip file yang telah didownload didalam directory download
```c
    child_id3 = fork();
    if (child_id3 == 0)
    {
        sleep(3);
        char *argv[]={"unzip", "hewan","-d","download",NULL};
        execv("/bin/unzip", argv);
    }
```

### b
`
Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
`
mengambil gambar random
```c
char* random_image(char* folder_path) {
    DIR* dir = opendir(folder_path);
    if (!dir) {
        printf("Error: could not open directory.\n");
        return NULL;
    }
    
    // count the number of image files in the directory
    int count = 0;
    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strstr(entry->d_name, "_")) {
            count++;
        }
    }
    
    // select a random image file
    srand(time(NULL));
    int random_index = rand() % count;
    rewinddir(dir);
    int current_index = 0;
    char* random_filename = NULL;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strstr(entry->d_name, "_")) {
            if (current_index == random_index) {
                random_filename = strdup(entry->d_name);
                break;
            } else {
                current_index++;
            }
        }
    }
    
    closedir(dir);
    return random_filename;
}
```

### c
`
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
`
buat directory berdasarkan jenis hewan
```c
    pid_t child_createdir_darat, child_createdir_amphibi, child_createdir_air;

    child_createdir_darat = fork();
    if (child_createdir_darat < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_darat == 0 )
    {
        char *argv[] = {"mkdir", "-p", "HewanDarat",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_amphibi = fork();
    if (child_createdir_amphibi < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_amphibi == 0 )
    {
        char *argv[] = {"mkdir", "-p", "HewanAmphibi",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_air = fork();
    if (child_createdir_air < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_air == 0 )
    {
        char *argv[] = {"mkdir", "-p", "HewanAir",NULL};
        execv("/bin/mkdir", argv);
    }
```
Kemudian pindahkan file gambar berdasarkan tempat tinggal
```c
    pid_t move;
    move = fork();
    if (move == 0)
    {
        while(image_count>0){
            char* folder_path = "download/";
            char* random_filename = random_image(folder_path);
            if (random_filename) {
                printf("Selected image: %s\n", random_filename);
                char* habitat = get_habitat(random_filename);
                if(strcmp(habitat, "darat")==0){
                    char *args[] = {"find", "download", "-type", "f", "-iname", "*$random_filename*", "-exec", "mv", "{}", "HewanDarat", ";", NULL};
                    int result = execv("/bin/mv", args);
                }
                if(strcmp(habitat, "air")==0){
                    char *args[] = {"find", "download", "-type", "f", "-iname", "*$random_filename*", "-exec", "mv", "{}", "HewanAir", ";", NULL};
                    int result = execv("/bin/mv", args);
                }
                if(strcmp(habitat, "amphibi")==0){
                    char *args[] = {"find", "download", "-type", "f", "-iname", "*$random_filename*", "-exec", "mv", "{}", "HewanAmphibi", ";", NULL};
                    int result = execv("/bin/mv", args);
                }
                free(random_filename);
            } else {
                printf("Error: could not select random image.\n");
            }
            image_count--;
        }
    }
    waitpid(move, &status, 0);
```

### d
`
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.
`
zip directory yang telah dibuat untuk menghemat penyimpanan
```c
    child_zip = fork();
    if (child_zip < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_zip == 0 )
    {   
        sleep(2);
        char *argv[] =   {"zip", "-r", "HewanJaga.zip", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/usr/bin/zip", argv);
    }
```


## Soal 2
### a
`
Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
`
```c
while (1) {
    pid_t mkdir_child_id;

        //Direktori setiap 30 detik
        time_t now = time(NULL);
        time(&now);
        struct tm* time_info;
        time_info = localtime(&now);
        char timestamp[60];
        strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);
        //printf("timestamp : %s\n", timestamp);

        mkdir_child_id = fork();

        if (mkdir_child_id < 0) 
            exit(EXIT_FAILURE);

        if (mkdir_child_id == 0) {
            execlp("/bin/mkdir", "mkdir", timestamp, NULL);
            exit(0);
        }

```
.
```
        sleep(30);
```

Untuk soal a, format diambil dari localtime yang diberi format `%Y-%m-%d_%H:%M:%S` kemudian nilai di assign ke dalam variabel yang nanti akan menjadi nama directory. Pada `execlp("/bin/mkdir", "mkdir", timestamp, NULL);` akan dibuat directory menggunakan perintah mkdir yang nama foldernya berdasar pada variabel yang sebelumnya disebutkan di atas. Untuk memberi jarak per 30 detik, diberikan `sleep(30)` supaya antar folder terdapat jarak waktu pembuatan tiap perulangan.

### b 
`
Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
`

```c
pid_t predownload_id = fork();
            if (predownload_id < 0){
                exit(EXIT_FAILURE);
            }
            if (predownload_id == 0){
                int i;
                for (i = 0; i < 15; i++)
                //download 15 gambar dengan looping
                {
                    pid_t download_id = fork();

                    if (download_id < 0){
                        exit(EXIT_FAILURE);
                    }

                    if (download_id == 0) {
                        now = time(NULL);
                        
                        char str[5];
                        char link[60];
                        sprintf(link, "https://picsum.photos/%ld", (now%1000)+50);
                        //printf("link : %s\n", link);

                        time_t time_pict;
                        struct tm *time_now_pict;
                        char files[30];
                        char down_dir[150];

                        time(&time_pict);
                        time_now_pict = localtime(&time_pict);

                        strftime(files, sizeof(files), "%Y-%m-%d_%H:%M:%S", time_now_pict);
                        sprintf(down_dir, "%s/%s", timestamp, files);

                        // printf("%s\n", down_dir);
                        char *arg[] = {"wget", "-q", "-O", down_dir, link, NULL};
                        execv("/bin/wget", arg);              
                    }
                    sleep(5); 
                }

```
Pada snippet kode ini, `sleep(5)` akan memberi jarak pendownloadan antar foto dan foto akan didownload sebanyak 15 kali per folder dengan bantuan `for (i = 0; i < 15; i++)` melalui 
```c
char *arg[] = {"wget", "-q", "-O", down_dir, link, NULL};
execv("/bin/wget", arg);  
```
dimana down_dir merupakan variabel merupakan path yang membantu kita menaruh hasil download foto ke dalam folder terkait serta pemberian nama dari foto itu sendiri yang mana mengikuti format yang didapat melalui format terkait `%Y-%m-%d_%H:%M:%S`.
Untuk pendownloadan ke link yang disediakan, dibantu oleh variabel bernama link yang di belakang nya akan ditambahkan ukuran dari foto yang akan didownload, terlihat pada bagian kode `sprintf(link, "https://picsum.photos/%ld", (now%1000)+50);` untuk penyematan link yang diharapkan dan nanti digunakan pada `execv` yang terlihat di snippet di atas

### c
`
Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
`
```c
int status;
                wait(&status);
                char zip_name[100];
                sprintf(zip_name, "%s.zip", timestamp);
                pid_t zip_id = fork();

                if (zip_id < 0){
                    exit(EXIT_FAILURE);
                }
                if (zip_id == 0){
                    char *arg[] = {"zip", zip_name, "-rm", timestamp, NULL};
                    execv("/bin/zip", arg);
                }

```
Untuk perapian, akan dilakukan zip folder terkait jika sudah 15 gambar terdownload, hal ini dapat diwujudkan dengan menaruh kode snippet di atas setelah perulangan download di soal b yang mana pada 
```c
char *arg[] = {"zip", zip_name, "-rm", timestamp, NULL};
execv("/bin/zip", arg);
```
terlihat argumen untuk membentuk zip yaitu `zip` dan argumen `rm` untuk menghapus folder yang telah di zip kemudian argumen di gunakan pada `execv("/bin/zip", arg);`

### d dan e
`
Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
`

```c
     if (argc == 2) {
        FILE* kill_soal2 = fopen("kill.sh", "w");
        fprintf(kill_soal2, "#!/bin/bash\n");
        if (!strcmp("-a", argv[1])) {
            fprintf(kill_soal2, "killall -9 lukisan\nrm $0\n");
        } else if (!strcmp("-b", argv[1])) {
            fprintf(kill_soal2, "kill_parent(){\n");
		    fprintf(kill_soal2, "kill ${@: -1}\n");
		    fprintf(kill_soal2, "}\n");
		    fprintf(kill_soal2, "kill_parent $(pidof lukisan)\nrm $0\n");
        }

        fclose(kill_soal2);
        
        pid_t cid = fork();
        if (cid < 0){
            exit(EXIT_FAILURE);
        }
        if (cid == 0){
            execlp("bin/chmod", "chmod", "+x", "kill.sh", NULL);
            exit(0);
        }

```
Untuk mengenerate program killer berdasarkan argumen pengeksekusian file, makan digunakan conditional if untuk membantu mendeterminasi kondisi terkait, sebagai contoh untuk mengambil argumen -a digunakan `if (!strcmp("-a", argv[1]))`. Untuk tipe a, kami mengincar bagian file eksekusi dengan asumsi nama file exe yang terbentuk nanti adalah sama dengan file c nya yaitu lukisan, maka perintah `fprintf(kill_soal2, "killall -9 lukisan\nrm $0\n");` akan mematikan process bernama lukisan yang mana akan memberhentikan semua process yang berlangsung. Untuk tipe b, kami akan mematikan parent dari melalui 
```c
		    fprintf(kill_soal2, "kill ${@: -1}\n");
		    fprintf(kill_soal2, "}\n");
		    fprintf(kill_soal2, "kill_parent $(pidof lukisan)\nrm $0\n");
```
Sehingga ketika mengambil pid dari lukisan dan menjalankan kill_parent, akan diambil pid parent melalui `${@: -1}` lalu di kill supaya child masih bisa menyelesaikan tugasnya terlebih dahulu.
Setelah pembuatan kill.sh (bash file), supaya dapat di akses dengan bash pada terminal maka dilakukan 
`execlp("bin/chmod", "chmod", "+x", "kill.sh", NULL);`. 

Agar bisa berjalan layaknya Daemon maka diberi snippet kode berikut
```c
pid_t pid, sid;

        pid = fork();

        if (pid < 0) {
            exit(EXIT_FAILURE);
        }
        
        if (pid > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);

        sid = setsid();
        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

    }

```

## Soal 3
### a
`
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip 
`
download file database pemain
```c
    child_download = fork();
    if (child_download == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "-O", "pemain", NULL};

        execv("/usr/bin/wget", argv);
    }

```
unzip file
```c
    child_unzip = fork();
    if (child_unzip == 0)
    {
        sleep(3);
        char *argv[]={"unzip", "pemain",NULL};
        execv("/bin/unzip", argv);
    }
```
hapus file zip
```c
    child_remove_pemain = fork();
    if (child_remove_pemain < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_remove_pemain == 0 ){
        sleep(4);
        char *argv[] = {"rm", "-rf", "pemain", NULL};
        execv("/bin/rm", argv);
    }
```

### b
`
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
`
hapus semua penain yang bukan dari manchester united
```c
    child_remove_selain_mu = fork();
    if (child_remove_selain_mu < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_remove_selain_mu == 0 ){
        sleep(4);
        
        char *argv[] = {"find", "players", "-type", "f", "!", "-name", "*ManUtd*.png", "-delete", NULL};
        execv("/bin/find", argv);
    }
```

### c
`
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
`
buat folder kategori
```c
    pid_t child_createdir_kiper, child_createdir_bek, child_createdir_gelandang, child_createdir_penyerang;

    child_createdir_kiper = fork();
    if (child_createdir_kiper < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_kiper == 0 )
    {
        char *argv[] = {"mkdir", "-p", "players/kiper",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_bek = fork();
    if (child_createdir_bek < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_bek == 0 )
    {
        char *argv[] = {"mkdir", "-p", "players/bek",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_gelandang = fork();
    if (child_createdir_gelandang < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_gelandang == 0 )
    {
        char *argv[] = {"mkdir", "-p", "players/gelandang",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_penyerang = fork();
    if (child_createdir_penyerang < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_penyerang == 0 )
    {
        char *argv[] = {"mkdir", "-p", "players/penyerang",NULL};
        execv("/bin/mkdir", argv);
    }
```

pindahkan pemain ke posisi mereka dalam waktu bersamaan
```c
    pid_t child_move_kiper, child_move_bek, child_move_gelandang, child_move_penyerang;
    while (wait(NULL) > 0);

    child_move_kiper = fork();
    if (child_move_kiper < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_move_kiper == 0 )
    {
        char *argv[] = {"find", "players", "-type", "f", "-iname", "*Kiper*", "-exec", "mv", "{}", "players/kiper", ";", NULL};
		execv("/bin/find", argv);
    }
    else
    {
        while(wait(&status) > 0 );
    }

    child_move_bek = fork();
    if (child_move_bek < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_move_bek == 0 )
    {
        char *argv[] = {"find", "players", "-type", "f", "-iname", "*Bek*", "-exec", "mv", "{}", "players/bek", ";", NULL};
		execv("/bin/find", argv);
    }
    else
    {
        while(wait(&status) > 0 );
    }

    child_move_gelandang = fork();
    if (child_move_gelandang < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_move_gelandang == 0 )
    {
        char *argv[] = {"find", "players", "-type", "f", "-iname", "*Gelandang*", "-exec", "mv", "{}", "players/gelandang", ";", NULL};
		execv("/bin/find", argv);
    }
    else
    {
        while(wait(&status) > 0 );
    }

    child_move_penyerang = fork();
    if (child_move_penyerang < 0 )
    {   
        exit(EXIT_FAILURE);
    }
    else if (child_move_penyerang == 0 )
    {
        char *argv[] = {"find", "players", "-type", "f", "-iname", "*Penyerang*", "-exec", "mv", "{}", "players/penyerang", ";", NULL};
		execv("/bin/find", argv);
    }
    else
    {
        while(wait(&status) > 0 );
    }

```

### d
`
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
`
fungsi buat tim
```c
void buatTim(int a, int b, int c) {
    char filename[50];
    sprintf(filename, "Formasi_%d-%d-%d.txt", a, b, c);
    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("Error creating file!\n");
        return;
    }
    fclose(fp);
}
```
## Soal 4

`
Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

Bonus poin apabila CPU state minimum.
`
```c
if (argc == 5){
        if (!strcmp(argv[1], "*")){
            seconds=-1;
            //printf("%d\n", seconds);
        }
        else{
            seconds=atoi(argv[1]);
            //printf("%d\n", seconds);
            if (seconds>= 60){
                printf("Error Format");
                exit(0);
            }
        }

        if (!strcmp(argv[2], "*")){
            minutes=-1;
            //rintf("%d\n", minutes);
        }
        else{
            minutes=atoi(argv[2]);
            //printf("%d\n", minutes);
            if (minutes >= 60){
                printf("Error Format");
                exit(0);
            }
        }
        //printf("%s", argv[3]);
        if (!strcmp(argv[3], "*")){
            hours=-1;
            //printf("%d\n", hours);
        }
        else{
            hours=atoi(argv[3]);
            //printf("%d\n", hours);
            if (hours >= 24){
                printf("Error Format");
                exit(0);
            }
        } 

        strcpy(path,argv[4]);
```
.
```c
 }else{
        printf("Error Format");
        exit(0);
    } 
```

Kode di atas akan mengambil argumen dan menolak beberapa hal yang mungkin menyebabkan error, seperti detik, menit, jam diluar range yang ditentukan lalu mengeluarkan error. 

Supaya berjalan dalam background, maka ditambahkan 
```c
pid_t pid, sid;

        pid = fork();

        if (pid < 0) {
            exit(EXIT_FAILURE);
        }
        
        if (pid > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);

        sid = setsid();
        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
```
Dan untuk cron sendiri diatur dalam 
```c
while(1){
        time_t now = time(NULL);
        struct tm* time_now;
        time_now = localtime(&now);
        int checker=1;
        //printf("%d %d %d", time_now->tm_hour, time_now->tm_min, time_now->tm_sec);
        if (hours == -1 && minutes == -1 && seconds == -1){
            checker=1;
        }else {
            if(hours != -1){
                if(time_now->tm_hour != hours) checker=0;
            }
            if (minutes != -1){
                if(time_now->tm_min != minutes) checker=0;
            }
            if (seconds != -1){
                if(time_now->tm_sec != seconds) checker=0;
            }
        }
        //printf("Checker : %d\n", checker);
        if (checker > 0){
            pid_t make_child_id = fork();
            if (make_child_id < 0) {
                exit(EXIT_FAILURE);
            }
            if (make_child_id == 0){
                char * arg[] = {"bash", path, NULL};
                execv("/bin/bash", arg);
            }
        }
    }
```
Dimana pada bagian bawah, eksekusi dari program akan dilakukan sesuai dengan konfigurasi cron yang dimasukkan oleh pengguna melalui 
```c
char * arg[] = {"bash", path, NULL};
execv("/bin/bash", arg);
```
