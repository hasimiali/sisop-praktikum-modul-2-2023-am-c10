#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>

#ifndef DT_REG
#define DT_REG 8
#endif

char* get_habitat(char* filename) {
    char* delimiter = "_";
    char* token;
    char* habitat;
    token = strtok(filename, delimiter); // get the first token
    token = strtok(NULL, delimiter); // get the second token
    if (token != NULL) {
        habitat = strtok(token, "."); // remove the file extension
        return habitat;
    }
    return NULL;
}


int count_images(char* folder_path) {
    DIR* dir = opendir(folder_path);
    if (!dir) {
        printf("Error: could not open directory.\n");
        return -1;
    }
    
    int count = 0;
    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        struct stat st;
        char path[1024];
        sprintf(path, "%s/%s", folder_path, entry->d_name);
        if (stat(path, &st) == 0 && S_ISREG(st.st_mode) && strstr(entry->d_name, ".jpg")) {
            count++;
        }
    }
    
    closedir(dir);
    return count;
}

char* random_image(char* folder_path) {
    DIR* dir = opendir(folder_path);
    if (!dir) {
        printf("Error: could not open directory.\n");
        return NULL;
    }
    
    // count the number of image files in the directory
    int count = 0;
    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strstr(entry->d_name, "_")) {
            count++;
        }
    }
    
    // select a random image file
    srand(time(NULL));
    int random_index = rand() % count;
    rewinddir(dir);
    int current_index = 0;
    char* random_filename = NULL;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strstr(entry->d_name, "_")) {
            if (current_index == random_index) {
                random_filename = strdup(entry->d_name);
                break;
            } else {
                current_index++;
            }
        }
    }
    
    closedir(dir);
    return random_filename;
}

int main(){
    pid_t child_id1, child_id2, child_id3;
    int status;

    // Download file using wget
    child_id1 = fork();
    if (child_id1 == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "hewan", NULL};

        execv("/usr/bin/wget", argv);
    }

    // Wait for file to finish downloading
    waitpid(child_id1, &status, 0);

    // Create directory
    child_id2 = fork();
    if (child_id2 == 0)
    {
        char *argv[] = {"mkdir", "download", NULL};
        execv("/bin/mkdir", argv);
    }

    // Unzip file
    child_id3 = fork();
    if (child_id3 == 0)
    {
        char *argv[] = {"unzip", "hewan", "-d", "download", NULL};
        execv("/usr/bin/unzip", argv);
    }



    // Wait for child processes to finish
    waitpid(child_id3, &status, 0);

//buat folder

    pid_t child_createdir_darat, child_createdir_amphibi, child_createdir_air;

    child_createdir_darat = fork();
    if (child_createdir_darat < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_darat == 0 )
    {
        char *argv[] = {"mkdir", "-p", "HewanDarat",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_amphibi = fork();
    if (child_createdir_amphibi < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_amphibi == 0 )
    {
        char *argv[] = {"mkdir", "-p", "HewanAmphibi",NULL};
        execv("/bin/mkdir", argv);
    }

    child_createdir_air = fork();
    if (child_createdir_air < 0 )
    {
        exit(EXIT_FAILURE);
    }
    else if (child_createdir_air == 0 )
    {
        char *argv[] = {"mkdir", "-p", "HewanAir",NULL};
        execv("/bin/mkdir", argv);
    }

    char* folder_path = "download/";
    int image_count = count_images(folder_path);
    pid_t count;
    count = fork();
    if (count == 0)
    {
        printf("Number of images: %d\n", image_count);
    }
    waitpid(count, &status, 0);

    pid_t move;
    move = fork();
    if (move == 0)
    {
        while(image_count>0){
            char* folder_path = "download/";
            char* random_filename = random_image(folder_path);
            if (random_filename) {
                printf("Selected image: %s\n", random_filename);
                char* habitat = get_habitat(random_filename);
                if(strcmp(habitat, "darat")==0){
                    char *args[] = {"find", "download", "-type", "f", "-iname", "*$random_filename*", "-exec", "mv", "{}", "HewanDarat", ";", NULL};
                    int result = execv("/bin/mv", args);
                }
                if(strcmp(habitat, "air")==0){
                    char *args[] = {"find", "download", "-type", "f", "-iname", "*$random_filename*", "-exec", "mv", "{}", "HewanAir", ";", NULL};
                    int result = execv("/bin/mv", args);
                }
                if(strcmp(habitat, "amphibi")==0){
                    char *args[] = {"find", "download", "-type", "f", "-iname", "*$random_filename*", "-exec", "mv", "{}", "HewanAmphibi", ";", NULL};
                    int result = execv("/bin/mv", args);
                }
                free(random_filename);
            } else {
                printf("Error: could not select random image.\n");
            }
            image_count--;
        }
    }
    waitpid(move, &status, 0);

    return 0;
}
